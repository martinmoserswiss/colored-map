angular.module('coloredMap', ['ngRoute', 'ngSanitize'])

    .controller('MapCtrl', function ($scope) {

        $("#draggable").draggable();

        var canvas = document.getElementById('canvas_picker').getContext('2d');
        var selectedCanton;

        // create an image object and get it’s source
        var img = new Image();
        img.src = 'img/donut.png';

        // Copy the image to the canvas
        $(img).load(function () {
            canvas.drawImage(img, 0, 0);
        });

        function rgbToHex(R, G, B) { return toHex(R) + toHex(G) + toHex(B) }

        function toHex(n) {
            n = parseInt(n, 10);
            if (isNaN(n)) return "00";
            n = Math.max(0, Math.min(n, 255));
            return "0123456789ABCDEF".charAt((n - n % 16) / 16) + "0123456789ABCDEF".charAt(n % 16);
        }

        // Hide color chooser when the user is clicking on a unknown area.
        $(document).click(function (event) {
            let clickedArea = event.target.id;
            if (clickedArea === "" || clickedArea === "map" || clickedArea === "mapContainer" || clickedArea === "functionBoard") {
                $('#draggable').hide();
            }
        });

        // Color all
        $scope.colorAll = function () {
            selectedCanton = undefined;
            var x = $('#colorAllButton').offset().left;
            var y = $('#colorAllButton').offset().top - $('#map').height();
            $('#draggable').css({ 'top': y, 'left': x })
            $('#draggable').show();
        }

        // Clear all
        $scope.clearAll = function () {
            $('.canton').attr('fill', '#fff');
        };

        // Download final image
        $scope.download = function () {

            // Get the final svg to download.
            let svg = $('#map')[0].outerHTML;

            // Create a new virtual canvas.
            var canvas = document.createElement("canvas");
            canvas.width = 1011;
            canvas.height = 649;

            // Create a new virtual image.
            var img = document.createElement("img");
            img.setAttribute("src", "data:image/svg+xml;base64," + btoa(svg));

            img.onload = function () {
                // Create a virtual image.
                canvas.getContext("2d").drawImage(img, 0, 0);

                // Create a virtual link.
                let a = document.createElement('a');
                a.href = canvas.toDataURL("image/png")
                a.download = 'colored-map.png';
                a.target = '_blank';

                // Append the virtual image to the DOM, click it and let the user download it.
                document.body.appendChild(a); a.click();

                // Remove the virtual link from the DOM.
                document.body.removeChild(a);
            };
        };

        $scope.createLegend = function () {
            alert('coming soon 🤓');
        }

        // Color the choosen canton with the clicked color.
        $('#draggable').click(function (event) {

            // getting user coordinates
            var x = event.pageX - this.offsetLeft;
            var y = event.pageY - this.offsetTop;

            // getting image data and RGB values
            var img_data = canvas.getImageData(x, y, 1, 1).data;
            var R = img_data[0];
            var G = img_data[1];
            var B = img_data[2];

            // convert RGB to HEX
            var hex = rgbToHex(R, G, B);
            if (hex != "3F3844" && hex != "000000") {
                if (selectedCanton === undefined) {
                    $('.canton').attr('fill', '#' + hex)
                } else {
                    $(selectedCanton).attr('fill', '#' + hex)
                }
            }
        });

        function registerClickEvents(el) {

            // Recognize the clicked canton and show the chooser.

            $(el).click(function (event) {
                // getting user coordinates
                var x = event.pageX;
                var y = event.pageY - $('#map').height();

                let type = $(el).attr('class');
                if (type == 'canton') {
                    selectedCanton = $(el);
                    $('#draggable').css({ 'top': y, 'left': x })
                    $('#draggable').show();
                }
            });

            // Set white color to the corresponding canton if the user doubleclicks this corresponding canton.
            $(el).dblclick(function (event) {
                $('#draggable').hide();
                let type = $(el).attr('class');
                if (type == 'canton') {
                    $(el).attr('fill', '#FFFFFF')
                }
            });
        }

        // Register events
        $('img.svg').each(function () {
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            $.get(imgURL, function (data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Add replaced image's ID to the new SVG
                if (typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if (typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass + ' replaced-svg');
                }

                $svg = $svg.removeAttr('xmlns:a');

                // Replace image with new SVG
                $img.replaceWith($svg);

                $('path').each(function () {
                    registerClickEvents
                        (this);
                });

                $('polygon').each(function () {
                    registerClickEvents
                        (this);
                });

                $('polyline').each(function () {
                    registerClickEvents
                        (this);
                });
            });
        });

    })

    .directive('navBar', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/navBar.html'
        };
    })

    .directive('footerTemplate', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/footerTemplate.html',
            controller: function ($scope) {
                // logic
            }
        }
    })

    .config(function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'templates/map.html'
        })
        /*
        .when('/ueberuns', {
            templateUrl: 'templates/ueberuns.html',
            controller: 'UeberunsCtrl'
        })
        */
    });
