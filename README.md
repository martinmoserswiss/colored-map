# Colored swiss map

a map colorer

![](exampleMap.png)

## Abstract

It let's the user create different colored Swiss maps.
A map is divided by cantons, lakes and boarders where the user can customize each canton.

## Access

The app is available under http://159.203.4.229/coloredSwissMap/

## Server

Following are the details to the project running on the server.

<b>Folder</b><br/>
The repository is located under

```
/home/dinu/apps/coloredSwissMap/
```

where the final app is located under

```
/home/dinu/apps/coloredSwissMap/coloredSwissMap
```

<b>Nginx configuration</b><br/>
The proxy server has a new location entry which points to

```
/home/dinu/apps/coloredSwissMap/
```
